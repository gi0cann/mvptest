from flask import Flask, request, make_response, render_template
from flask.ext.bootstrap import Bootstrap
from flask.ext.script import Manager
from random import choice
from string import lowercase

app = Flask(__name__)
bootstrap = Bootstrap(app)
manager = Manager(app)
app.config["DEBUG"] = True


def rand_string(n):
    return "".join(choice(lowercase) for i in xrange(n))

list_of_cookies = list()
for i in xrange(0, 6):
    string1 = rand_string(10)
    string2 = rand_string(13)
    list_of_cookies.append([string1, string2])


@app.route('/', methods=["GET", "POST"])
def index():
    name = request.form.get('name')
    required = request.cookies.get('required')
    cookie2 = request.cookies.get('cookie2')
    cookie3 = request.cookies.get('cookie3')
    cookie4 = request.cookies.get('cookie4')
    if required == "True" and cookie2 == "True" and cookie3 == "True" and cookie4 == "True":
        return render_template('index.html', name=name)
    resp = make_response(render_template('index.html'))
    resp.set_cookie('required', 'True')
    resp.set_cookie('cookie2', 'True')
    for cookie in list_of_cookies[:3]:
        resp.set_cookie(cookie[0], cookie[1])
    resp.set_cookie('cookie3', 'True')
    for cookie in list_of_cookies[3:]:
        resp.set_cookie(cookie[0], cookie[1])
    resp.set_cookie('cookie4', 'True')
    return resp

if __name__ == '__main__':
    manager.run()
